%Moments of inertia of the motors
Jm1=5.e-3;
Jm2=2.e-3;

%Pur avendolo calcolato, non sar� incorporato nel modello del robot, ma sar� separato, quindi
%metteremo 0 come 

%Stiffness constants of the transmissions
Kel1=70;
Kel2=70;
Kel=diag([Kel1,Kel2]);

%Viscous friction coefficients of the transmissions
Del1=0.05;
Del2=0.05;

%Reduction ratios
n1=100;
n2=100;
N=diag([n1,n2]);

%Robot definitions

L1=link([0,1,0,0,0]);
L1.m=50;
L1.r=[-0.5,0,0];
L1.I=[0,0,0;0,0,0;0,0,10];
L1.G=1;
L1.Jm=0;
%Note:  motors and transmissions are defined outside the block which 
%simulates the robot


L2=L1;

r2=robot({L1,L2});

r2.name='POLIrobot';
r2.gravity=[0,9.81,0];


%SIMULINK MODEL IMPLEMENTS THIS EQUATION
%J_m1 * ddq_m1 = tau_m1 - tau_lm1 //No link viscous friction term (D_m1 * dq_m1)
%tau_lm1 = H_el1 * (q_m1 - (n * q_l1)) + D_el1 * (dq_m1 - n*dq_l1)
%The same for the second motor



%Calcolo del modello dinamico per il calcolo della q_m di riferimento per
%la trajectory planning

% SEE PHOTO 2

%Instant of brake release instant
ton=0.1;

%Initial instant of motion
t0=0.5;

%Travel time
T=0.5;

%Maximum speed
sd=5;

%Distance to cover
h=1.6;

%Acceleration time
ta=(T*sd-h)/sd;

%Maximum acceleration
amax=sd/ta;

%Initial value for the trajectory
s0=0.2;

%Initial reference position at the joints
qrif0=ikine(r2,[eye(3),[0.2;0;0];[0,0,0,1]],[-1,3],[1 1 0 0 0 0 ]);

%Initial motor positions
qm0=qrif0*N; %Moltiplicata per il reduction ration N

%Initial link positions
q0=fsolve(@(q0) N*Kel*(qm0'-N*q0')-gravload(r2,q0)',qrif0); %Risoluzine dell'eq. implicita

%Load side torques at steady state
tau0=gravload(r2,q0);

%Motor side torques at steady state
taum0=tau0/N;


%Load side reference values of the moments of inertia
Jl1=60;
Jl2=22.5;

%Same values referred to the motor axes
Jlr1=Jl1/n1^2;
Jlr2=Jl2/n2^2;

%Inertia ratios
ro1=Jlr1/Jm1;
ro2=Jlr2/Jm2;

%Parameters: axis 1 - STUDIATE A LEZIONE!

%Gv_m1(s) = 

%mu/s * 
%(1 + 2 * (csi_z1/w_z1) * s + s^2/(w_z1^2)) / (1 +2*(csi_p1/w_p1) * s + s^2/(w_p1^2)))

%R_v1(s) = Kp_v1 * (1 + 1/(s*Ti_v1)) = Kp_v1 * (1 +s*Ti_v1) / (s*Ti_v1)

%1/Ti_v1 = w_z1 / 10 -->  Ti_v1 = 10 / w_z1

%Kp_v1? ROOT LOCUS! Ma ce lo far� il pc :)

mu1=1/(Jm1+Jlr1);
wz1=sqrt(Kel1/Jlr1);
csiz1=0.5*Del1/sqrt(Jlr1*Kel1);
wp1=sqrt(1+ro1)*wz1;
csip1=sqrt(1+ro1)*csiz1;
Gvm1=tf(mu1*[1/wz1^2 2*csiz1/wz1 1],[1/wp1^2 2*csip1/wp1 1 0]);

%Tuning: axis 1 controller

Tiv1=10/wz1;
Kiv1=10.4; %From rltool, we copied the PARAMETERS ABOVE, then make rltool execute, 
%then maximize the damping and then finding the Integral Gain of the
%controller
Kpv1=Kiv1*Tiv1;

%wTILDE_cv1 = Kpv1 * mu1 / w_z1;
wcv1=Kpv1*mu1;
wtildecv1=wcv1/wz1;

Rv1 = tf([Kpv1 Kiv1],[1 0]); %% Kpv * (1+s*Tiv) / Tiv;
Lv1=Gvm1*Rv1; %Loop Transfer Function
Fv1=feedback(Lv1,1); %Lv(s) / (1 + Lv(s)) funzione di sensitivit�
Gpm1 = Fv1*tf(1,[1 0]); %Integra (tf(1,[1 0]) la Fv1: Transfer function without Kpp

%SEE PHOTO 1

Kpp1=28.7; %From rltool

%Parameters: axis 2

mu2=1/(Jm2+Jlr2);
wz2=sqrt(Kel2/Jlr2);
csiz2=0.5*Del2/sqrt(Jlr2*Kel2);
wp2=sqrt(1+ro2)*wz2;
csip2=sqrt(1+ro2)*csiz2;
Gvm2=tf(mu2*[1/wz2^2 2*csiz2/wz2 1],[1/wp2^2 2*csip2/wp2 1 0]);

%Tuning: axis 2 controller

Tiv2=10/wz2;
Kiv2=10.4; %From rltool
Kpv2=Kiv2*Tiv2;
wcv2=Kpv2*mu2;
wtildecv2=wcv2/wz2;

Rv2 = tf([Kpv2 Kiv2],[1 0]);
Lv2=Gvm2*Rv2;
Fv2=feedback(Lv2,1);
Gpm2 = Fv2*tf(1,[1 0]);
Kpp2=45; %From rltool
