% Generating a random (Erdos-Renyi), undirected network

clear all

% randseed=fix(clock)*[0 3 5 7 11 13]';
% rand('state',randseed);
% rand('state',0);

N=10000;     %total number of nodes
k=4;        %average node degree
p=k/(N-1);   %probability of connection;

A=sparse(N,N);      %connectivity matrix
deg=zeros(1,N);     %node degree vector

%build the connectivity matrix
nlinks=N*k/2;  %number of links to be set up
for i=1:nlinks
        
    if rem(i,100)==0
        disp(['Creating link ',int2str(i),' of ',int2str(nlinks)])
    end;
    
    repeat=1;
    while repeat 
        %extracting two nodes
        n1=round(rand*N+0.5);
        n2=round(rand*N+0.5);
        if (rand<p)&&(n1~=n2)&&(A(n1,n2)==0)
            A(n1,n2)=1;
            A(n2,n1)=1;
            repeat=0;
        end;
    end;
end;

%final degree vector
deg=sum(A);
avgdeg=full(mean(deg));
maxdeg=full(max(deg));
mindeg=full(min(deg));
disp(['Average degree = ',num2str(avgdeg),' (Expected = ',num2str(k),')'])
disp(['Max degree = ',num2str(maxdeg),'    Min degree = ',num2str(mindeg)])

%degree distribution
degrees=mindeg:maxdeg;
h=hist(deg,maxdeg-mindeg+1);     
h=h/sum(h);
plot(degrees,h)

%saving the connectivity matrix
% save NetERlogn3k4.mat A N deg
% spy(A)