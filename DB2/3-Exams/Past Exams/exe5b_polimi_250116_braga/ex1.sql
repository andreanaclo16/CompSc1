create trigger updatePortfolioValue
after insert on TradeValues
for each row

declare diff MONEY

begin 

/* diff in value */
select (new.Value - (select Value from TradeValues
 	where ( ItemId = new.Itemid and Timestamp = 
 	(select max(Timestamp) from TradeValues
 	where Timestamp < new.Timestamp and ItemId = new.Itemid)))) into diff

update SubPortfolio SP
set SP.Value = SP.Value + diff *
				(select Qty from Holding H where
				SP.PortfolioId = H.PortfolioId and H.Itemid = new.Itemid)

update MainPortfolio MP
set MP.Value = MP.Value + diff *
				(select Qty from Holding H where
				MP.PortfolioId = H.PortfolioId and H.Itemid = new.Itemid)


end


create trigger updateSubPortfolio
after update on Value of SubPortfolio
for each row

begin

update SubPortfolio SP
set SP.Value = (SP.Value - old.Value) + new.Value
where SP.PortfolioId = new.PortfolioUp

update MainPortfolio MP
set MP.Value = (MP.Value - old.Value) + new.Value
where MP.PortfolioId = new.PortfolioUp

end

create trigger updateMainPortfolio
after update on Value of MainPortfolio
for each row

begin

update Customer C
set C.FinancialWealth = (C.FinancialWealth - old.Value) + new.Value
where C.CustomerId = new.OwnerCustormer

end


create trigger insertHolding
after insert on Holding
for each row 

declare currentValue MONEY

begin 

select Value into currentValue from TradeValues
 	where ( ItemId = new.Itemid and Timestamp = 
 	(select max(Timestamp) from TradeValues 
 	and ItemId = new.Itemid))

update SubPortfolio SP
set SP.Value = SP.Value + new.Qty * currentValue
where SP.PortfolioId = new.PortfolioUp

update MainPortfolio MP
set MP.Value = MP.Value + new.Qty * currentValue
where MP.PortfolioId = new.PortfolioUp

end


create trigger deleteHolding
after delete on Holding
for each row 

declare currentValue MONEY

begin 

select Value into currentValue from TradeValues
 	where ( ItemId = new.Itemid and Timestamp = 
 	(select max(Timestamp) from TradeValues 
 	and ItemId = new.Itemid))

update SubPortfolio SP
set SP.Value = SP.Value - old.Qty * currentValue
where SP.PortfolioId = new.PortfolioUp

update MainPortfolio MP
set MP.Value = MP.Value - old.Qty * currentValue
where MP.PortfolioId = new.PortfolioUp

end


create trigger updateHolding
after update on Holding
for each row 

declare currentValue MONEY

begin 

select Value into currentValue from TradeValues
 	where ( ItemId = new.Itemid and Timestamp = 
 	(select max(Timestamp) from TradeValues 
 	and ItemId = new.Itemid))

update SubPortfolio SP
set SP.Value = SP.Value - old.Qty * currentValue + new.Qty * currentValue
where SP.PortfolioId = new.PortfolioUp

update MainPortfolio MP
set MP.Value = MP.Value - old.Qty * currentValue + new.Qty * currentValue
where MP.PortfolioId = new.PortfolioUp

end





